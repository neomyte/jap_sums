# Jap 4eme session

## Vocabulaire

| Japanese   | Hiragana      | Translation   |
|:----------:|:-------------:|:-------------:|
| 送る        | おくる         | envoyer       |
| 最強        | さいきょう      | le plus fort  |
| 身勝手      | みがって        | égoïste       |
| 大切        | たいせつ       | important, nécessaire, indispensable   |
| 動く        | うごく         | bouger, faire fonctionner, faire quelque chose |
| 閉める       | しめる         | fermer, éteindre   |
| 遊ぶ        | あそぶ         | jouer     |
| 顔          | かお           | visage, expression (du visage), influence|
| 娘          | むすめ          | fille (daughter)     |
| 帰る         | かえる         | retourner, rentrer   |
| 島          | しま           | île   |
| 着く        | つく           | arriver, atteindre   |
| 技          | わざ          | technique, art, capacité, compétence   |
| 名前         | なまえ        | nom   |
| 書く         | かく         | écrire, composer (dessiner => 描く)  |
| 草むら        | くさむら      | herbes, hautes herbes (pk)   |

## Grammaire - Verbes

### **Substantiver une phrase**

Parfois, on se retrouve dans une situation où l'on souhaite parler d'une action plutôt que de simplement la décrire. En français, on a souvent recours à l'expression "le fait de...", par exemple : "Je n'aime pas le fait que tu sois arrivé en retard.".  
En Japonais, on utiliser la particule `の` collée au verbe final de la phrase pour créer cette forme. Cette particule doit ensuite être suivie de la particule qui donne son rôle à l'action, par exemple : `は`, `が` et `を`.

Ainsi, la phrase peut être le thème d'une nouvelle phrase (`は` ou `が`) : 
> **日本語を話す** (nihongo wo hanasu) => *je parle japonais*  
> **日本語を話すのが** (nihongo wo hanasu no ga) => *parler japonais*  
> **日本語を話すのが下手です** (nihongo wo hanasu no ga heta desu) => *Je ne suis pas doué pour parler japonais. / Je ne parle pas bien japonais.*

```mermaid
graph LR
    subgraph phrase
        subgraph subordonnée
            日本語 -. を .-> 話す
        end
        話す -. のが .-> 下手です
    end
```

Ou elle peut être l'objet de la nouvelle phrase (`を`) :  
> **魚を料理する** (sakama wo ryouri suru) => *je cuisine du poisson*  
> **魚を料理するのを** (sakama wo ryouri suru no wo) => *cuisiner du poisson*  
> **魚を料理するのを勉強する** (sakama wo ryouri suru no wo benykou suru) => *J'apprends à cuisiner du poisson.*

```mermaid
graph LR
    subgraph phrase
        subgraph subordonnée
            魚 -. を .-> 料理する
        end
        料理する -. のを .-> 勉強する
    end
```

### **Subordonnées**

Faire une subordonnée en Japonais est assez simple, il suffit de coller la subordonnée au nom dont elle dépend en préfixe : 
> **昨日買った** (kinou katta) => *J'ai acheté hier*  
> **魚を食べる** (sakana wo taberu) => *Je mange du poisson*  
> **昨日買った魚を食べる** (kinou katta sakana wo taberu) => *Je mange le poisson que j'ai acheté hier.*

```mermaid
graph LR
    subgraph phrase
        subgraph subordonnée
            昨日買った
        end
        昨日買った --> 魚
        魚 -. を .-> 食べる
    end
```

### **Avoir déjà fait**

En utilisant le principe des subordonnées couplé avec le mot [事](https://jisho.org/search/koto) (chose, fait, occurence) et `ある` (être), on peut assez facilement parler de choses que l'on a déjà faites : 
> **日本に行った** (nihon ni itta) => *être allé au Japon*  
> **日本に行った事** (nihon ni itta koto) => *Le fait d'être allé au Japon*  
> **日本に行った事があります** (nihon ni itta koto ga arimasu) => *Le fait que je sois allé au Japon existe. / Je suis déjà allé au Japon.*

```mermaid
graph LR
    subgraph phrase
        subgraph subordonnée
            日本 -. に .-> 行った
        end
        行った --> 事 
        事 -. が .-> あります
    end
```


## Grammaire - Adjectifs/Vocabulaire

<ins>Les "asokodo":</ins>

Asokodo c'est le nom que je leur donne, il y en a sûrement un autre. ^^  

Il s'agit d'une série de quatre mots qui se décline de manière assez uniforme pour former des mots plutôt utiles : 
 * `あX` : Parle d'une chose au loin
 * `そX` : Parle d'une chose qui est hors de portée, qui est impalpable
 * `こX` : Parle d'une chose qui est à portée
 * `どX` : Pose une question sur une chose

Le `X` peut prendre plusieurs valeurs dont : 
 * `う` : la manière (exception: ああ au lieu de あう)
 * `こ` : le lieu
 * `れ` : la chose
 * `ちら` : la direction
 * `の` : décale le sens sur le mot qui suit

Pas très clair, n'est-ce pas ?  
On va essayer de changer ça avec quelques exemples (genre tous en faite) : 
| Mot   | Sens      | Explication   |
|:----------:|:-------------:|:-------------:|
| `これ` | ça/ceci/cela | une *chose* qui est *à portée* |
|`それ` | ça/ceci/cela | une *chose* qui est *hors de portée* | 
|`あれ` | ça là-bas | une *chose* qui est *au loin* | 
|`どれ` | laquelle/lequel ? | *question* sur une *chose* | 
|`こう` | comme ça | la *manière* qui est *à portée* (là où je suis, ce que je fais) |
|`そう` | comme ça | la *manière* qui est *hors de portée* (que je vois/constate/devine/comprends) | 
|`ああ` | comme ça | la *manière* qui est *au loin* (que je vois/constate/devine/comprends) | 
|`どう` | comment ? | *question* sur la *manière* | 
|`ここ` | ici | un *lieu* qui est *à portée* | 
|`そこ` | là | un *lieu* qui est *hors de portée* (que je pointe) | 
|`あこ` | là-bas | un *lieu* qui est *au loin* | 
|`どこ` | où ? | *question* sur le *lieu* | 
|`こちら` | par ici | une *direction* qui est *à portée* | 
|`そちら` | par là | une *direction* qui est *hors de portée* (que je pointe) | 
|`あちら` | par là-bas | une *direction* qui est *au loin* | 
|`どちら` | par où ? | *question* sur la *direction* | 
|`この猫` | ce chat | un *chat* qui est *à portée* | 
|`その猫` | ce chat là | un *chat* qui est *hors de portée* | 
|`あの猫` | ce chat là-bas | un *chat* qui est *au loin* | 
|`どの猫` | quel chat ? | *question* sur le *chat* | 



## Grammaire - Particules



<ins>**と** (lister des noms):</ins>

La particule `と` a de nombreux usages qui nécessiteront plusieurs leçons. Dans celle-ci nous nous contenterons de voir comment lister des noms avec.  
Son usage est plutôt simple, il suffit de l'utiliser comme "et" en français : 
> **バナナと牛乳が好きですか** (banana to gyuunyuu ga suki desu ka) => *Aimes-tu le lait et les bananes ?*  

Attention, à n'utiliser que pour lister des noms. Pour les adjectifs, les verbes, il faut rester avec la `て form`.

```mermaid
graph LR
    subgraph phrase
        subgraph liste
            バナナ -. と .-> 牛乳
        end
        牛乳 -. が .-> 好きです
        好きです -. か .-> question[[Question]]
    end
```

<ins>**とか** (lister des noms):</ins>

La particule `とか` fonctionne comme la particule `と` à deux choses prêtes : 
 * Elle liste des noms pour définir un "genre de chose" plutôt qu'une réelle liste de chose concrète.
 * On peut répéter `とか` à la fin de la liste pour appuyer sur le fait qu'il y a d'autres chose, un peu comme une sorte de "etc...".

Un petit exemple : 
> **オタクはアニメとか漫画とかが好きな人ですね** (otaku ha anime toka manga toka ga suki na hito desu ne) => *Les otakus, c'est des gens qui aiment les animes les mangas et les trucs dans le genre, hein ?*

```mermaid
graph LR
    subgraph phrase
        オタク -. は .-> アニメ
        subgraph liste
            アニメ -. とか .-> 漫画
            漫画 -. とか .-x fin[[Fin de la liste]]
        end
        fin -. が .-> 好き
        好き -.  な .-> 人です
        人です -. か .-> question[[Question]]
    end
```
