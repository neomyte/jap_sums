# Jap 1ere session



## Grammaire - Introduction



La grammaire en Japonais va majoritairement se diviser en 4 catégories : 

* L'utilisation de certains **mots de vocabulaire**
* La conjugaison/utilisation des **verbes**
* La conjugaison/utilisation des **adjectifs**
* L'utilisation des **particules**



## Grammaire - Verbes

<INS>Deux types de verbes :</ins>

* **-ru verb**: aussi appelés :
  * ichidan verb (一段動詞, ichidan doushi)
  * -iru, -eru verb : intéressant pour se rappeler qu'il faut le son `e` ou `i` avant le `る`
* **-u verb**: aussi appelés :
  *  godan verb (五段動詞, godan doushi)

<ins>Le stem (la racine) :</ins>

| ru verb   | u verb |
|:----------:|:-------------:|
| <strike>`る`</strike>  | <strike>u</strike>+**i** |
| 食べる -> 食べ | 泳ぐ -> 泳ぎ |
| (taberu -> tabe) | (oyogu -> oyogi) |

<ins>La masu (`ます`) form :</ins>

**Forme permettant de passer d'informel à formel.**

> Stem + `ます`

| ru verb   | u verb |
|:----------:|:-------------:|
| 食べる -> 食べ -> 食べます | 泳ぐ -> 泳ぎ -> 泳ぎます |
| (taberu -> tabe -> tabemasu) | (oyogu -> oyogi -> oyogimasu) |



<ins>Le cas `する` :</ins>

する (suru) est presque toujours irrégulier. Mais, en réalité, ses irrégularités suivent un pattern simple.  
Il suffit, pour n'importe qu'elle conjugaison, de le traiter comme le verbe imaginaire `す`. (shi, shimasu, shite, shita, etc...)  
C'est bien plus simple que de s'embêter à apprendre une à une les exceptions. Seule exception à cette règle est la forme potentielle (出来る), mais c'est particulier et on verra ça plus tard. 



## Grammaire - Adjectifs

<ins>Utilisation comme verbe :</ins>

Un adjectif peut s'utiliser un peu comme un verbe pour faire une phrase simple :   

* 食べる (taberu ; manger) -> `Je mange`
* 好き (suki ; aimé) -> J'aime ça / Je t'aime



<ins>です :</ins>

です (desu) sert à passer de la forme informelle à formelle dans le cas d'utilisation de l'adjectif comme un verbe. です n'est PAS le verbe être.   

| Informelle | Formelle |
|:----------:|:-------------:|
| 好き | 好き**です** |
| suki | suki desu |

On peut remarquer le す final comme pour la ます forme. On s'accorde à dire que です vient probablement de mutiples formes : `で<stem>ます`.   
Typiquement : `であります` que l'on pourrait, pour le coup, traduire par le verbe être. D'où la profonde confusion.  

<INS>Deux types d'adjectifs' :</ins>

| i adjectives   | na adjectives |
|:----------:|:-------------:|
| Se termine par un い (le caractère, pas le son)  | Les autres... |
| Collé au nom | Collé au nom via `な` (na) |
| 可愛**い**女の子 | 綺麗**な**女の子 |
| kawaii onna no ko | kirei na onna no ko |
| femme mignonne | belle femme |

Le `な` n'apparaît que quand on colle l'adjectif à un nom (ou autre point à voir plus tard).  

Aussi :
 * 可愛いです (c'est mignon)
 * 綺麗です (c'est beau, pas de na)

PS : 女 (onna) signifie femme, mais utilisé seul c'est plutôt péjoratif (un peu comme si je disais femelle en français), donc, `女の子` et autres joyeusetés sont des habitudes à prendre. ^^  



## Grammaire - Particules



<ins>は :</ins>

Prononcé `wa`. 

A gardé cette prononciation dans des expressions qui sont devenus des mots :  

* `こんにちは` (今日は) (konnichiha ; konnichiwa)
* `こんばんは` (今晩は) (konbanha ; konbanwa)

Sert à définir le topique/sujet de la phrase. (On ne parle pas du sujet grammatical !!)  
  
On pourrait traduire par "à propos de ...".  

* 犬は好き (inu ha suki) : `à propos de chien, aimé` -> `J'aime les chiens.`
* 先生は忙しいです (sensei ha isogashii desu): `à propos de professeur, occupé` -> `Le professeur est occupé.` (Les débats sur les traductions de sensei sont légions, mais on va s'en tenir à ça ^^)



<ins>が :</ins>

Cette particule porte vite à confusion car elle a un rôle quasi identique à `は`.   
Contrairement à ce qui est souvent dit, ce n'est toujours PAS le sujet grammatical (nominatif ou autre).   
  
が apporte deux différences :   

* On change l'insistance sur ce qui vient avant le `が` (c'est l'inverse pour le は)
* On vient apporter une information nouvelle au discours



Une façon efficace, en français, de traduire cette double notion est la syntaxe : `C'est ... qui/que ...` :  

* 犬が好き (inu ga suki) : `C'est chien, que aimé` -> `C'est les chiens que j'aime.` 
* 先生が忙しいです (sensei ga isogashii desu): `C'est professeur, qui occupé` -> `C'est le professeur qui est occupé.`



Dans le cas de mot interrogatif, on utilisera du coup toujours が. Prenons `何`(nani, quoi, que, what).  

* 何は好きです (nani ha suki desu) : `à propos de quoi, aimé` -> Ca ne veut rien dire
* 何が好きです (nani ga suki desu) : `C'est quoi, que aimé` -> Ca ressemble tout de suite plus à quelque chose

PS : Les questions peuvent (et sont souvent) accompagnés de `か` à la fin. On s'attendra donc plus à la phrase : `何が好きですか`. (nani ga suki desu ka)  



<ins>の :</ins>

Fonctionne comme le `'s` en anglais ou pour préciser un rôle :  

* 友達のけいち (tomodachi no keichi) : Mon ami Keichi
* 日本の車 (nihon no kuruma) : Voiture du japon (japonaise)
* 私の友達 (watashi no tomodachi) : Mon ami (moi's ami)



<ins>を :</ins>

Prononcé `o`.  

Sert pour définir l'object d'un verbe transitif :   

* 林檎を食べる (ringo wo taberu) -> `Je mange une pomme`.
* コークを飲みます (kooku wo nomimasu) -> Je bois du Coca-Cola. (formel)

