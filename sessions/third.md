# Jap 3eme session

## Vocabulaire

| Japanese   | Hiragana      | Translation   |
|:----------:|:-------------:|:-------------:|
| 食べる      | たべる         | manger        |
| 待つ        | まつ          | attendre      |
| 説明        | せつめい       | explication   |
| 経験値      | けいけんち      | expérience   |
| 傷薬        | きずぐすり      | potion       |
| 効果        | こうか         | efficacité   |
| 使う        | つかう         | utiliser     |
| 買う        | かう           | acheter      |
| 戦う        | たたかう       | combattre     |
| 元気        | げんき         | vigoureux, sain, en bonne santé   |
| 選ぶ        | えらぶ         | choisir, sélectionner   |
| 拾う        | ひろう         | ramasser, trouver   |
| 勝負        | しょうぶ        | match, défaite ou victoire   |
| 来る        | くる           | venir   |
| 博士        | はかせ         | professeur   |
| 地方        | ちほう         | region, district   |

## Grammaire - Verbes

### **Les suffixes**

Les verbes peuvent être trouvés assez souvent avec des suffixes. Ces suffixes se collent au `stem` du verbe.  
Parfois, le suffixe est un autre verbe qui permet de former un nouveau verbe (e.g. 引く(hiku, tirer) + 出す(dasu, sortir) => 引き出す(hikidasu) => extraire, faire ressortir, retirer).

<ins>[Suffixe ~たい](https://jisho.org/search/%E3%81%9F%E3%81%84%20%23sentences):</ins>

Le suffixe `たい` permet de définir le volitif, la volonté de faire :
> 使う (utiliser) => 使いたい (vouloir utiliser)  
> 殺す (tuer) => 殺したい (vouloir tuer)  
> 勉強する (étudier) => 勉強したい (vouloir étudier)  

<ins>[Suffixe ~物](https://jisho.org/search/mono):</ins>

En lui-même `物`(mono) a de nombreuses significations qui tournent autour de `la chose`.  
En tant que suffixe verbial, il sert à substantivé le verbe pour donner l'object ou le résultat de l'action :
> 買う (acheter) => 買い物 (shopping, achat)  
> 食べる (manger) => 食べ物 (nourriture)  
> 着る (porter (des épaules au bas)) => 着物 (kimono, vêtement) 

### **Utiliser la て form**

Maintenant que l'on sait former la `て form`, reste à connaître ses différents usages. Tous ne seront pas vu d'une traite.

<ins>ている form:</ins>

Il existe deux verbes "être" en japonais : `いる`(居る) et `ある`(有る).
いる est le verbe être pour les êtres animés.  
Et il peut servir, **couplé à la て form**  à créer la `ている form`.  

Cette forme sert à exprimer une action courante, qui se produit en ce moment : 
> 泳ぐ (nager) => 泳いでいる ((Je suis) en train de nager.)  
> 行く (aller) => 行っている ((Je suis) en train d'y aller.)

Cette forme peut être et est souvent raccourcies : `てる`. On aurait alors : 
> 泳いでいる => 泳いでる  
> 行っている => 行ってる

On peut remarquer que dans un cas (te**iru**) comme dans l'autre (t**eru**), on a une terminaison classique de `る verb`.  
Il est donc possible de conjuguer cette forme comme n'importe quel `る verb`, par exemple, au passé : 
> 泳ぐ (nager) => 泳いでいた/泳いでた ((J'étais) en train de nager.)  
> 行く (aller) => 行っていた/行ってた ((J'étais) en train d'y aller.)

<ins>Actions simultanées:</ins>

La `て form` sert également à définir des actions simultanées en les enchaînant simplement : 
 * **野菜を食べて死んだ** (yasai wo tabete shinda) => *Je suis mort en mangeant des légumes.*
 * **帰って、ピカチュウのぬいぐるみを買いました** (kaette, pikachuu no nuigurumi wo kaimashita) => *J'ai acheté une peluche Pikachu en retrant.*

```mermaid
graph LR
    subgraph 野菜を食べて死んだ
        subgraph manger_legume
            野菜 -. を .-> 食べて
        end
        食べて --> 死んだ
        subgraph mourir
            死んだ
        end
    end
    subgraph 帰ってピカチュウのぬいぐるみを買いました
        subgraph rentrer
            帰って
        end
        帰って --> ピカチュウ
        subgraph acheter_peluche
            subgraph peluche
                ピカチュウ -. の .-> ぬいぐるみ
            end
            ぬいぐるみ -. を .-> 買いました
        end
    end
```

<ins>Actions successives:</ins>

La `て form` sert également à définir des actions successives. Il n'y a pas vraiment de distinction avec les actions simulatanées. C'est une déduction selon le contexte :
 * **野菜食べて死んだ** (yasai tabete shinda) => *J'ai mangé des légumes et je suis mort.*
 * **帰って、ピカチュウのぬいぐるみを買いました** (kaette, pikachuu no nuigurumi wo kaimashita) => *Je suis rentré et j'ai acheté une peluche Pikachu.*

> Cf graphe précédent, les phrases sont identiques.

## Grammaire - Adjectifs

<ins>Les adverbes:</ins>

Tout adjectif en japonais peut être facilement transformé en adverbes :

|  い adjectives  | な adjectives |
|:---------------:|:-------------:|
| <strike>`い`</strike>+**く**  | <strike>`な`</strike>+**に** |
| 遅い -> 遅**く** | 元気 -> 元気**に**(e.g. なる) |
| osoi -> osoku | genki -> genki ni (e.g. naru) |

<ins>La て form:</ins>

Maintenant que l'on connait quelques usages de la `て form`, il serait intéressant de savoir comment la former pour des adjectifs : 

|  い adjectives  | な adjectives |
|:---------------:|:-------------:|
| <strike>`い`</strike>+**くて**  | +**で** |
| 遅い -> 遅**くて** | 元気 -> 元気**で** |
| osoi -> osokute | genki -> genki de |




## Grammaire - Particules



<ins>**から** et **まで**:</ins>

La particule `から` sert à définir l'origine au sens large, cela peut donc être, par exemple :
 * le point de départ
 * la cause
 * la raison 

Quelques exemples : 
 * **午前三時から暇がある** (gozen sanji kara hima ga aru) => *J'ai du temps libre à partir de trois heures du matin.*
 * **彼は利己的な男ですから本当に嫌いです** (kare ha rikoteki no otoko desu kara hontou ni kirai desu) => *Je le déteste vraiment parce que c'est un (homme) égoïste.*
 * **明日忙しいから先に行くよ** (ashita isogashii kara saki ni iku yo) => *Je suis occupé demain, donc je pars (le premier) maintenant.*

```mermaid
graph LR
    subgraph 午前三時から暇がある
        午前三時 -. から .-> 暇
        暇 -. が .-> ある
    end
    subgraph 彼は利己的な男ですから本当に嫌いです
        subgraph homme_egoiste
            彼 -. は .-> 利己的
            利己的 -. な .-> 男です
        end
        男です -. から .-> 本当
        subgraph deteste_vraiment
            本当 -. に .-> 嫌いです
        end
    end
    subgraph 明日忙しいから先に行くよ
        明日忙しい -. から .-> 先
        先 -. に .-> 行く
        行く -. よ .-> yo[[n'est-ce pas ?]]
    end
```

Lorsque l'on défini un point de départ, on peut aussi avoir envie de définir le point de fin, pour cela, on utilise `まで` :
 * **午前三時から<ins>午前六時まで</ins>暇がある** (gozen sanji kara gozen rokuji made hima ga aru) => *J'ai du temps libre à partir de trois heures du matin à 6 heures du matin.*

```mermaid
graph LR
    subgraph 午前三時から午前六時まで暇がある
        subgraph début_fin
            午前三時 -. から .-> début[[Début]]
            午前六時 -. まで .-> fin[[Fin]]
            début --> durée
            fin --> durée[[Durée]]
        end
        durée --> 暇
        暇 -. が .-> ある
    end
```

<ins>**ので**:</ins>

La particule `ので` est assez similaire à `から` tant dans l'usage que le sens mais porte uniquement sur **la raison et la cause**.  
Ainsi la phrase `明日忙しいから先に行くよ` vue précédemment pourrait également être : **明日忙しい<ins>ので</ins>先に行くよ**.  

Petite différence, lorsque précédé d'un `な adjective`, il faut ajouter le `な` : 
 * **彼は身勝手<ins>なので</ins>本当に嫌いです** (kare ha migatte na node hontou ni kirai desu) => *Je le déteste vraiment parce qu'il est égoïste.*

<ins>**けど** (+けれど、けれども、けども、けんど) et **が** :</ins>

> Les formes entre parenthèses ont toutes le même sens et le même fonctionnement que けど, ce sont juste des variantes en fonction de la formalité que l'on souhaite apporter.

La particule `けど` (ou `が` dans un contexte plus formel) peut se placer à la fin d'une phrase (comme から pour une cause dans le deuxième exemple plus haut) pour traduire nos **"mais", "cependant", "malgré tout"**.

Quelques exemples : 
 * **忙しいですけど来る** (isogashii desu kedo kuru) => *Je suis occupé, mais je viendrai (quand même).*
 * **午前3時ですけど食べたいです** (gozen sanji desu kedo tabetai desu) => *Il est trois heures du matin, mais j'ai envie de manger.*

```mermaid
graph LR
    subgraph 忙しいですけど来る
        subgraph occupé
            忙しいです
        end
        忙しいです -. けど .-> 来る
    end
    subgraph 午前3時ですけど食べたいです
        subgraph 3am
            午前3時です
        end
        午前3時です -. けど .-> 食べたいです
    end
```