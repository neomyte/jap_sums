# Jap 2eme session



## Grammaire - Verbes



<ins>La て form:</ins>

Elle a de nombreux usage que l'on ne verra pas pour le moment.

| ru verb   |
|:----------:|
| <strike>`る`</strike>+て  |
| 食べる -> 食べて |
| (taberu -> tabete) |

| -す (su)   | -む, -ぶ, - ぬ (mu, bu, nu) | -ぐ, -く (gu, ku) | -る, -う, -つ (ru, u, tsu) |
|:----------:|:-------------:|:-------------:|:---------------:|
| <strike>`す`</strike>+**して**  | <strike>む, ぶ, ぬ</strike>+**んで** | <strike>ぐ</strike>+**いで**; <strike>く</strike>+**いて** | <strike>る, う, つ </strike>+**って** |
| 殺す -> 殺して | 学ぶ -> 学んで | 泳ぐ -> 泳いで | 分かる -> 分かって |
| (korosu -> koroshite) | (manabu -> manande) | (oyogu -> oyoide) | (wakaru -> wakatte) |
|  | 読む -> 読んで | 書く -> 書いて | 言う -> 言って |
|  | (yomu-> yonde) | (kaku -> kaite) | (iu -> itte) |
| | 死ぬ -> 死んで |    | 待つ -> 待って |
|  | (shinu-> shinde) |  | (matsu -> matte) |



<ins>Le cas **する**:</ins>

>  する -> して

<ins>La **た** form:</ins>

Permet de définir le passé. (Il n'y a que deux temps en japonais, passé et présent)

> て form + (て -> た / で -> だ)

<ins>La négation:</ins>

**Dans le cas informel :**

| ru verb   | u verb |
|:----------:|:-------------:|
| <strike>`る`</strike>+**ない**  | <strike>u</strike>+**a**+**ない** |
| 食べる -> 食べない | 泳ぐ -> 泳がない |
| (taberu -> tabenai) | (oyogu -> oyoganai) |

Exception : pour les verbes se terminant par "う" :
> う ->  わ (e.g. 言う -> 言わない (iu -> iwanai))

**Dans le cas formel :**

> ~ます -> ~ません



<ins>La négation passée :</ins>

!! A chaque fois, en partant de la négation. !!

**Dans le cas informel :**

> な<strike>`い`</strike>+**かった**

Exemples : 
> 食べない -> 食べなかった  
> 泳がない -> 泳がなかった

**Dans le cas formel :**

> \+ でした

Exemples : 
> 食べません -> 食べませんでした  
> 泳ぎません -> 泳ぎませんでした



## Grammaire - Adjectifs

<ins>La négation:</ins>

**Dans le cas informel :**

|  い adjectives  | な adjectives |
|:----------:|:-------------:|
| <strike>`い`</strike>+**くない**  | + **じゃない** |
| 忙しい -> 忙しくない | 綺麗 -> 綺麗じゃない |
| (isogashii -> isogashiku nai) | (kirei -> kirei ja nai) |

**Dans le cas formel :**

> です -> ではありません

Notes (pour aller plus loin) :  
En réalité, il n'y a pas de formes négatives des adjectifs.
Dans tous les cas de négation, on se base sur la négation de ある, à savoir ない (informel) ou ありません (formel).  
Dans le cas des い adjectives, on prend en fait des adverbes (<strike>`い`</strike>+**く**) et on ajoute ない.  
Donc, théoriquement, la 忙しくありません serait tout autant valide (même si jamais employé (ou du moins jamais vu)).  
Dans le cas des な adjectives, on fonctionne comme avec les noms, on cumule les particules で et は et on ajoute la négation de ある.  
Sachant que `じゃ` est la contraction de `では`, toutes les formes suivantes sont valides : 
> 綺麗じゃない  
> 綺麗ではない  
> 綺麗じゃありません  
> 綺麗ではありません  

<ins>Le passé :</ins>

**Dans le cas informel :**

|  い adjectives  | な adjectives |
|:----------:|:-------------:|
| <strike>`い`</strike>+**かった**  | + **でした** |
| 忙しい -> 忙しかった | 綺麗 -> 綺麗でした |
| (isogashii -> isogashikatta) | (kirei -> kirei deshita) |

**Dans le cas formel :**

> です -> でした

Notes (pour aller plus loin) :  
La forme `かった` se forme comme la forme `くない`. Dans la forme くない, on ajoute ある à sa forme négative. Ici on ajoute sa forme passée あった.  
La différence est que dans ce cas-ci, la terminaison く de l'adverbe à fusionnée avec le verbe : 
> 忙しい -> 忙しく -> 忙しくあった -> 忙しかった

<ins>La négation passée :</ins>

!! A chaque fois, en partant de la négation. !!

**Dans le cas informel :**

> な<strike>`い`</strike>+**かった**

Exemples : 
> 忙しくない -> 忙しくなかった  
> 綺麗じゃない -> 綺麗じゃなかった


**Dans le cas formel :**

> \+ でした

Exemples : 
> 綺麗ではありません -> 綺麗ではありませんでした



## Grammaire - Particules



<ins>**に** vs **で**:</ins>

Dans les deux cas, on peut décrire un lieu ou un moment, mais ils ont aussi des sens distincts : 

|  に  | で |
|:----------:|:-------------:|
| endroit, temps où l'on est   | endroit, lieu où il se déroule qqchose |
| actions immédiates | moyen |
| destination | méthode |



<ins>**へ**:</ins>

Prononcé : `e`

Définit une direction.
