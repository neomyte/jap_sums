# Ressources

Ce fichier présente une liste de ressources recommandées pour l'apprentissage.

# Gratuites

Parmis les ressources gratuites recommandées se trouvent les suivantes : 
 * **Duolingo** (app) : intuitif, simple et pratique ; méfiance cependant sur les points de grammaires souvent trop vulgarisés voire faux
 * **KanjiSenpai** (app) : excellent pour apprendre les Kanji et du vocabulaire (pour avoir accès à plus de contenu audio il faut payer un peu ; on parle de quelque chose comme 3€ pour des centaines de samples, ce n'est pas la mort et n'est pas nécessaire)
 * **Anki** (app + desktop) : Excellente appli mais doit être accompagnée d'un deck ; je peux en fournir des très bons
 * **[Tae Kim](http://www.guidetojapanese.org/learn/grammar)** (app + web) : Parfait pour débuter la grammaire, un des meilleurs cours en ligne si ce n'est le meilleur
 * **[Jisho](https://jisho.org/)** (web) : Meilleur dictionnaire anglais/japonais en ligne
 * **JED** (app) : Meilleure (ou sûrement meilleure) appli de dictionnaire anglais/japonais
 * **[Japanese Ammo with Misa](https://www.youtube.com/channel/UCBSyd8tXJoEJKIXfrwkPdbA)** : Contrairement aux appareances, c'est probablement une des meilleures, si ce n'est la meilleure, chaîne de contenu pour apprendre le japonais sur youtube

# Payants

Les ressources payantes sont rarement de meilleures qualités que celles proposées par les passionnés. Je citerais tout de même **[NativeShark](https://www.nativshark.com)**. La qualité n'est pas toujours la meilleure mais le contenu et la bonne volonté sont là. Je partagerais peut-être des ressources à titre d'exemple.

# Partenaires

Pour trouver des partenaires, les classiques sont à privilégier : 
 * **[iTalki](https://www.italki.com)**
 * **Tandem** (app)
 * **[HelloTalk](https://www.hellotalk.com)**
 * **HiNative** (app) -> pour se faire corriger

# Autres mentions

 * **Les cours de japonais** (a.k.a. la chaine de Julien Fontanier) : Personnellement, je ne suis pas super fan de cette chaîne, majoritairement sur la partie grammaire. Il y a pas mal de choses qui me gênent. Cela dit, le mec explique bien et question contenu ça dose et ça a le mérite d'être en français, donc je le cite.
 * **Livres** : En livre, je recommande (mais c'est mega reuch, donc vous faites pas chier) : 
   * ***[A basic guide to Japanese grammar](https://www.bookdepository.com/A-Dictionary-of-Basic-Japanese-Grammar-Seiichi-Makino/9784789004541?redirected=true&utm_medium=Google&utm_campaign=Base1&utm_source=FR&utm_content=A-Dictionary-of-Basic-Japanese-Grammar&selectCurrency=EUR&w=AFF7AU9K7C2NV5A8VCZS&pdg=pla-317692435101:cmp-6484862834:adg-78060723815:crv-381328668200:pos-:dev-c&gclid=Cj0KCQjw2or8BRCNARIsAC_ppya3U9-wcIayBt_PSKYrU9BlLd75TA0NAv1j2eIb5wh7mqo7DOSzNQEaAovoEALw_wcB)***
   * ***[Kanji learner's dictionnary](https://www.bookdepository.com/The-Kodansha-Kanji-Learners-Dictionary-Jack-Halpern/9781568364070?redirected=true&utm_medium=Google&utm_campaign=Base1&utm_source=FR&utm_content=The-Kodansha-Kanji-Learners-Dictionary&selectCurrency=EUR&w=AFF7AU96RJJMFCA8VCZS&pdg=pla-317692435101:cmp-6484862834:adg-78060723815:crv-381328668200:pos-:dev-c&gclid=Cj0KCQjw2or8BRCNARIsAC_ppyZxhSN_f8wkF7U_wHMjp3Va-ECBM1ojbAvV2EUKdh84EdC0AVIvr_EaAixjEALw_wcB)***
 * **[Japanese Pod101](https://www.youtube.com/user/japanesepod101)** : Avis très mitigé sur cette chaîne. Le contenu est de qualité très variable. Leur site je déconseil à mort, c'est bourré de mensonge et ça vous spam méchamment. Mais faut reconnaître que sur la chaîne on tombe parfois sur des trucs sympas.
 * **FluentU** : Je n'ai pas encore testé, mais ça peut être pas mal, je mettrai à jour quand je me serai fait mon avis
 * **NHK Japanese news** : Faut avoir déjà bien les bases en japonais pour que cela en vaille la peine, mais pour s'entrainer, c'est sympas

# Vade Retro Satanas

 * **La japonais pour les nuls** : N'achetez JAMAIS cette merde. Ce truc est un receuil de phrase de merde de touriste mélangé avec des explications dégeulasses et malfoutues de la grammaire. Je ne parle même pas de la quantité de faute ! :O