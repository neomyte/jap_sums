# Sessions 

 * [Première session](sessions/first.md)
 * [Seconde session](sessions/second.md)
 * [Third session](sessions/third.md)
 * [Fourth session](sessions/fourth.md)

# Recommandations

 * [Recommandation en ressources](resources.md)
